// console.log("Hello World!")


// If, else if, else statement


let numG = -1;
// if statement 

if(numG < -5){
	console.log("Hello");
}

if(numG < 1){
	console.log("Hello");
}

//  else if statement
let numH = 1

if( numG > 0){
	console.log("Hello");
}else if(numH > 0){
	console.log("world!")
}


if (numG > 0){
	console.log("Hello")
} else if (numH == 0){
	console.log("world")
} else{
	console.log("Again")
}



let message = "No message";
console.log(message);

function determinTyphoonIntensity(windSpeed){

	if(windSpeed < 30){
		return "Not a typhoon yet";
	} else if (windSpeed<- 61){
		return "Tropical depression detected";
	} else if (windSpeed >= 62 && windSpeed <= 88){
		return "Tropical storm detected";		
	} else if (windSpeed >= 89 && windSpeed <= 117){
		return "Severe Tropical Storm detected";
	} else {
		return "Typhoon Detected";
	}

}

message = determinTyphoonIntensity(70);
console.log(message);

if (message == "Tropical depression detected"){
	console.warn(message);
}

message = determinTyphoonIntensity(112);
console.log(message);
message = determinTyphoonIntensity(120);
console.log(message);




if (true){
	console.log("truthy");
}
if (1){
	console.log("truthy")
}
if ([]){
	console.log("truthy")
}

if (false){
	console.log("falsy")
}

if (0){
	console.log("falsy")
}

if (undefined){
	console.log("falsy")
}


let ternaryResult1 = (1 < 18) ? true : false;
console.log("Result of ternary opertaror: " + ternaryResult1);
let ternaryResult2 = (100 < 18) ? true : false;
console.log("Result of ternary opertaror: " + ternaryResult2);

let name;

function isOfLegalAge(){
	name = "John"
	return "You are of the legal age limit";
}
function isOfUnderAge(){
	name = "Jane"
	return  "You are under the Age limit"

}

let age = parseInt(prompt("What is you age?"));
console.log(age);
console.log(typeof age);

let legalage = (age > 18) ? isOfLegalAge() : isOfUnderAge();
console.log("Result of ternary operator in function: " + legalage + ", " + name)


let day = prompt("What day of the week is it today?") .toLowerCase();
console.log(day);

switch(day){
case 'monday':
	console.log("The color of the day is red");
	break;
case 'tuesday':
	console.log("The color of the day is orange");
	break;
case 'wednesday':
	console.log("The color of the day is yellow");
	break;
case 'thursday':
	console.log("The color of the day is green");
	break;
case 'friday':
	console.log("The color of the day is blue");
	break;
case 'saturday':
	console.log("The color of the day is indigo");
	break;
case 'sunday':
	console.log("The color of the day is violet");
	break;
default:
	console.log("Please input a valid day");
	break;
}

function showIntensityAtert(windSpeed){
	try{
		alert (determinTyphoonIntensity(windSpeed))
	}catch (error){
		console.log(typeof error);
		console.warn(error.message);
	}finally{
		alert("Intensity updates will show new alert");
	}
}
showIntensityAtert(56);
